/*  rootb13     |   Program sum with function on C  |   09122020    */

/* Create a program that asks the user to type 2 whole numbers and add them,
 print the sums, use a function where you do the addition and printing.   */

#include <stdio.h>
#include <stdlib.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_RESET   "\x1b[0m"

void sum_basic(int a, int b){

    int c;

    c = a + b;

    printf(ANSI_COLOR_GREEN "\n\n\n\tLa suma de %d + %d es: %d\n", a, b, c);

};

int main(void){

    int a, b;
    char answere;

    do {
        system("clear");
        printf(ANSI_COLOR_YELLOW "\n\tEl programa realiza una suma con los 2 numeros enteros que proporciones.\n\n\n");

        printf(ANSI_COLOR_BLUE "\n\tIngrese el primer numero entero: ");
        scanf("%d", &a);

        printf(ANSI_COLOR_BLUE"\n\tIngrese el segundo numero entero: ");
        scanf("%d", &b);

        sum_basic(a, b);

        printf(ANSI_COLOR_RESET "\n\n\n\tDesea realizar otra suma?: ");
        scanf("\n%c", &answere);
    }while (answere == 's' || answere == 'S');

    system("clear");

    printf(ANSI_COLOR_RED"\n\n\tFIN DEL PROGRAMA\n\n\n");

    return 0;

}
